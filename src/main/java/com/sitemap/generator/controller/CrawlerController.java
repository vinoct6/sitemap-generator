package com.sitemap.generator.controller;


import com.sitemap.generator.crawler.WebCrawler;
import com.sitemap.generator.util.LinkExtractorHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Set;

import static java.util.Collections.singletonList;

@RestController
public class CrawlerController {

    @Autowired
    private WebCrawler webCrawler;

    @Autowired
    private LinkExtractorHelper linkExtractorHelper;

    @RequestMapping(value = "/crawl", method = RequestMethod.GET)
    public ResponseEntity<Collection<String>> crawl(@RequestParam(value = "url") String url) {

        if (!linkExtractorHelper.isHTTPLink(url)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(singletonList("Invalid URL"));
        }
        String sanitizedURL = linkExtractorHelper.removeForwardSlashIfPresentAtTheEnd(url);
        Set<String> crawledLinks = webCrawler.crawl(sanitizedURL);
        return ResponseEntity.ok(crawledLinks);
    }
}
