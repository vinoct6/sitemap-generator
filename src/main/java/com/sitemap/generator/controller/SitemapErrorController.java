package com.sitemap.generator.controller;


import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SitemapErrorController implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH, method = RequestMethod.GET)
    public String error() {
        String message = "Visit /crawl?url={url} to crawl the given domain url";
        return message;
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
