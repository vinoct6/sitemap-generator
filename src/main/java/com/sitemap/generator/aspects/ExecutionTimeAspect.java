package com.sitemap.generator.aspects;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class ExecutionTimeAspect {

    public static final String BEGIN_CRAWLING_WITH_URL = "Begin Crawling with URL %s";
    public static final String TIME_TAKEN_TO_CRAWL_THIS_DOMAIN_S_MILLISECONDS = "Time taken to crawl this domain : %d seconds.";
    private Logger logger = LoggerFactory.getLogger(ExecutionTimeAspect.class);

    @Around("execution ( * com.sitemap.generator.crawler.WebCrawler.crawl (*) )")
    public Set<String> calculateExecutionTime(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.nanoTime();
        logger.info(String.format(BEGIN_CRAWLING_WITH_URL, pjp.getArgs()[0]));
        Set<String> output = (Set<String>) pjp.proceed();
        long elapsedTime = System.nanoTime() - start;
        long seconds = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS);
        logger.info(String.format(TIME_TAKEN_TO_CRAWL_THIS_DOMAIN_S_MILLISECONDS, seconds));
        return output;
    }
}
