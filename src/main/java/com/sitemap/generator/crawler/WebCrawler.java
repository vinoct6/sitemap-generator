package com.sitemap.generator.crawler;

import com.sitemap.generator.util.LinkExtractorHelper;
import com.sitemap.generator.util.WebPageDownloadHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Service;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;


@Service
@EnableAspectJAutoProxy
public class WebCrawler {

    private Logger logger = LoggerFactory.getLogger(WebPageDownloadHelper.class);

    @Autowired
    private LinkExtractorHelper linkExtractorHelper;

    @Autowired
    private WebPageDownloadHelper webPageDownloadHelper;

    private List<String> visited = new ArrayList<>();

    private Queue<String> queue = new ArrayDeque<>();

    public Set<String> crawl(String domain) {

        queue.offer(domain);

        Set<String> links = new HashSet<>();

        while (!queue.isEmpty()) {
            String url = queue.remove();
            logger.info("Crawling : " + url);
            visited.add(url);

            Optional<String> htmlStringOptional = webPageDownloadHelper.getWebPageAsHTMLString(url);

            if (!htmlStringOptional.isPresent())
                continue;

            Set<String> urls = linkExtractorHelper.extractHyperLinks(htmlStringOptional.get());

            urls.forEach(u -> {
                if (webPageDownloadHelper.isMediaLink(u) || !webPageDownloadHelper.isSameDomain(u, domain)) {
                    links.add(u);
                } else if (webPageDownloadHelper.isRelativeURL(u)) {
                    String link = domain + u;
                    links.add(link);
                    enqueueLink(link);
                } else {
                    links.add(u);
                    enqueueLink(u);
                }
            });
        }
        return links;
    }

    private void enqueueLink(String link) {
        if (!visited.contains(link) && !queue.contains(link)
                && linkExtractorHelper.isHTTPLink(link))
            queue.offer(link);
    }
}
