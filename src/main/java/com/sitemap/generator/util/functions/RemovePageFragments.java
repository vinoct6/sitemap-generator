package com.sitemap.generator.util.functions;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.function.Function;


@Component
public class RemovePageFragments implements Function<String,String> {

    @Override
    public String apply(String s) {
        if(s.contains("#"))
            return StringUtils.substringBefore(s,"#");
        return s;
    }
}
