package com.sitemap.generator.util;


import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Helper class to download HTML content for a given URL
 */
@Component
public class WebPageDownloadHelper {

    static final String COULD_NOT_CONNECT_TO_URL_PLEASE_CHECK = "Could not connect to URL. Please Check";

    private static final String RELATIVE_URL_REGEX = "^(/.+/?)+";

    private Logger logger = LoggerFactory.getLogger(WebPageDownloadHelper.class);

    private List<String> validMediaLinks = Arrays.asList("pdf", "jpg", "jpeg", "mp3", "mp4", "gif", "doc", "docx", "ppt", "pptx");

    /**
     * Returns the HTML contents of the given URL.
     *
     * @param url
     * @return HTML page of the web page if the url is valid
     * empty String otherwise
     */
    public Optional<String> getWebPageAsHTMLString(String url) {

        if (isEmpty(url))
            return Optional.empty();

        return fetchHTMlStringForURL(url);
    }

    private Optional<String> fetchHTMlStringForURL(String urlString) {
        try {
            URL url = new URL(urlString);
            String htmlString = IOUtils.toString(url.openStream());
            return Optional.of(htmlString);
        } catch (IOException e) {
            logger.error(COULD_NOT_CONNECT_TO_URL_PLEASE_CHECK);
            return Optional.empty();
        }
    }

    public Boolean isSameDomain(String link, String domain) {
        if (StringUtils.contains(link, domain))
            return Boolean.TRUE;
        if (isRelativeURL(link))
            return Boolean.TRUE;
        return Boolean.FALSE;
    }

    public Boolean isRelativeURL(String url) {
        return url.matches(RELATIVE_URL_REGEX);
    }

    public Boolean isMediaLink(String link) {
        return validMediaLinks.stream().anyMatch(link::endsWith);
    }
}
