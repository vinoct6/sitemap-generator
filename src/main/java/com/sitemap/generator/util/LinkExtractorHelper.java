package com.sitemap.generator.util;

import com.sitemap.generator.util.functions.RemovePageFragments;
import com.sitemap.generator.util.functions.RemoveQueryParams;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;


@Component
public class LinkExtractorHelper {

    private static final String MATCH_ANCHOR_TAGS = "(?i)<a([^>]+)/?>((.+?)</a>)?";
    private static final String MATCH_HYPER_LINKS = "(?i)\\s*href=\\s*\"([^\"]*)\"";

    private Pattern anchorPattern = Pattern.compile(MATCH_ANCHOR_TAGS);
    private Pattern hyperLinksPattern = Pattern.compile(MATCH_HYPER_LINKS);

    @Autowired
    private RemoveQueryParams removeQueryParams;

    @Autowired
    private RemovePageFragments removePageFragments;


    public Set<String> extractHyperLinks(String htmlString) {
        Set<String> hyperLinks = new HashSet<>();

        Matcher anchorTagMatcher = anchorPattern.matcher(htmlString);

        while (anchorTagMatcher.find()) {
            String anchorTagMatch = anchorTagMatcher.group(1);
            Matcher hyperLinkMatcher = hyperLinksPattern.matcher(anchorTagMatch);
            if (hyperLinkMatcher.find()) {
                String url = hyperLinkMatcher.group(1);
                if (isNotEmpty(url))
                    hyperLinks.add(url);
            }
        }
        return sanitizeURLs(hyperLinks);
    }

    private Set<String> sanitizeURLs(Set<String> urls) {
        return urls.stream()
                .map(removeQueryParams.andThen(removePageFragments))
                .filter(url -> StringUtils.isNotEmpty(url) && !StringUtils.contains(url, "javascript:"))
                .collect(toSet());
    }

    public Boolean isHTTPLink(String url) {
        return StringUtils.isNotEmpty(url) && (url.toLowerCase().startsWith("http://") || url.toLowerCase().startsWith("https://"));
    }

    public String removeForwardSlashIfPresentAtTheEnd(String url) {
        if (url.endsWith("/"))
            return StringUtils.substring(url, 0, url.length() - 1);
        return url;
    }
}
