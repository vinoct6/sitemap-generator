package com.sitemap.generator.controller;


import com.sitemap.generator.crawler.WebCrawler;
import com.sitemap.generator.util.LinkExtractorHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.Collection;
import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class CrawlerControllerTest {

    @InjectMocks
    private CrawlerController controller;

    @Mock
    private WebCrawler webCrawler;

    @Mock
    private LinkExtractorHelper linkExtractorHelper;

    @Test
    public void shouldInteractWithLinkExtractor() {
        String url = "http://google.com";
        controller.crawl(url);
        verify(linkExtractorHelper).isHTTPLink(url);
    }

    @Test
    public void shouldReturnBadRequestForInvalidLinks() {
        String url = "abcd";
        when(linkExtractorHelper.isHTTPLink(url)).thenReturn(Boolean.FALSE);
        ResponseEntity<Collection<String>> output = controller.crawl(url);
        assertThat(output).isEqualTo(ResponseEntity.badRequest().body(Collections.singletonList("Invalid URL")));
    }

    @Test
    public void shouldCrawlLink() {
        String url = "http://google.com";
        when(linkExtractorHelper.isHTTPLink(url)).thenReturn(Boolean.TRUE);
        when(linkExtractorHelper.removeForwardSlashIfPresentAtTheEnd(url)).thenReturn(url);
        controller.crawl(url);
        verify(webCrawler).crawl(url);
    }
}