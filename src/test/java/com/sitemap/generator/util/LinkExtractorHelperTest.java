package com.sitemap.generator.util;

import com.sitemap.generator.util.functions.RemovePageFragments;
import com.sitemap.generator.util.functions.RemoveQueryParams;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class LinkExtractorHelperTest {

    @InjectMocks
    private LinkExtractorHelper linkExtractorHelper;

    @Spy
    private RemoveQueryParams removeQueryParams;

    @Spy
    private RemovePageFragments removePageFragments;

    @Test
    public void shouldExtractAllHyperLinksFromString() {

        StringBuilder inputString = new StringBuilder();

        inputString.append("<a href=\"http://www.nytimes.com/es/\" data-edition=\"spanish\">Español</a>").append("\n")
                .append("<a href=\"http://www.ycombinator.com?account=abc\">").append("\n")
                .append("Wipro Digital").append("\n")
                .append("<a href=\"/products/stratas-qms\">").append("\n")
                .append("Hello World").append("\n")
                .append("<a href=\"javascript:void(0)\" class=\"searchButton\">Search</a>").append("\n")
                .append("<a href=\"#\" class=\"toggle-nav\"><i class=\"fa fa-bars\"></i></a>").append("\n")
                .append("<a href=\"http://google.com/company#about-us\" class=\"toggle-nav\"><i class=\"fa fa-bars\"></i></a>").append("\n");

        Set<String> urls = linkExtractorHelper.extractHyperLinks(inputString.toString());

        assertThat(urls).containsExactlyInAnyOrder("http://google.com/company", "http://www.ycombinator.com",
                "/products/stratas-qms", "http://www.nytimes.com/es/");
    }

    @Test
    public void shouldValidateHTTPLink(){
        String url = "http://www.google.com";
        assertThat(linkExtractorHelper.isHTTPLink(url)).isTrue();

        url = "HTTPS://WWW.GOOGLE.COM";
        assertThat(linkExtractorHelper.isHTTPLink(url)).isTrue();
    }

    @Test
    public void shouldReturnFalseForInvalidURLs(){
        String url = null;
        assertThat(linkExtractorHelper.isHTTPLink(url)).isFalse();

        url = "abcd";
        assertThat(linkExtractorHelper.isHTTPLink(url)).isFalse();
    }

    @Test
    public void shouldRemoveForwardSlashIfPresentAtTheEnd(){
        String url = "http://www.google.com/";
        assertThat(linkExtractorHelper.removeForwardSlashIfPresentAtTheEnd(url)).isEqualTo("http://www.google.com");

        url = "http://www.google.com/";
        assertThat(linkExtractorHelper.removeForwardSlashIfPresentAtTheEnd(url)).isEqualTo("http://www.google.com");
    }
}