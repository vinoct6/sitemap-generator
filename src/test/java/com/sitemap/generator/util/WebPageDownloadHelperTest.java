package com.sitemap.generator.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class WebPageDownloadHelperTest {

    @InjectMocks
    private WebPageDownloadHelper webPageDownloadHelper;


    @Test
    public void shouldReturnEmptyOptionalIfURLIsNullOrEmpty() {
        Optional<String> htmlString = webPageDownloadHelper.getWebPageAsHTMLString(null);
        assertThat(htmlString.isPresent()).isFalse();
    }

    @Test
    public void shouldReturnEmptyOptionIfURLIsInvalid() throws Exception {
        String invalid_url = "invalid_url";
        Optional<String> htmlString = webPageDownloadHelper.getWebPageAsHTMLString(invalid_url);
        assertThat(htmlString.isPresent()).isFalse();
    }

    @Test
    public void shouldReturnTrueForRelationURL() {
        String url = "/products/our-product/";
        assertThat(webPageDownloadHelper.isRelativeURL(url)).isTrue();
    }

    @Test
    public void shouldReturnTrueIfSameDomain() {
        String domain = "http://spartasystems.com";
        String url1 = "http://spartasystems.com/about-us";
        String url2 = "http://spartasystems.jp/";
        String url3 = "http://twitter.com/spartasystems";

        assertThat(webPageDownloadHelper.isSameDomain(url1, domain)).isTrue();
        assertThat(webPageDownloadHelper.isSameDomain(url2, domain)).isFalse();
        assertThat(webPageDownloadHelper.isSameDomain(url3, domain)).isFalse();
    }

    @Test
    public void shouldReturnTrueIfRelativeURL() {
        String domain = "http://spartasystems.com";
        String url = "/products/our-product/";
        assertThat(webPageDownloadHelper.isSameDomain(url, domain)).isTrue();
    }

    @Test
    public void shouldIdentifyForMediaLinks() {
        String url1 = "http://spartasystems.com/about-us/aboutu-us.pdf";
        String url2 = "http://spartasystems.com/about-us";

        assertThat(webPageDownloadHelper.isMediaLink(url1)).isTrue();
        assertThat(webPageDownloadHelper.isMediaLink(url2)).isFalse();
    }
}