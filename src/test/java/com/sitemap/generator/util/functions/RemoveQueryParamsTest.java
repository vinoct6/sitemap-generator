package com.sitemap.generator.util.functions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class RemoveQueryParamsTest {

    @InjectMocks
    private RemoveQueryParams removeQueryParams;

    @Test
    public void shouldReturnTheSameStringIfURLHasNoQueryParams() {
        String url = "http://www.google.com/about-us";
        assertThat(removeQueryParams.apply(url)).isEqualTo(url);
    }

    @Test
    public void shouldRemoveQueryParamsIfURLHasThem() {
        String url = "http://www.google.com/about-us?name=Mark";
        assertThat(removeQueryParams.apply(url)).isEqualTo("http://www.google.com/about-us");
    }
}