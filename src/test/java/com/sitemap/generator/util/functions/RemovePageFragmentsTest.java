package com.sitemap.generator.util.functions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class RemovePageFragmentsTest {

    @InjectMocks
    private RemovePageFragments removePageFragments;

    @Test
    public void shouldReturnSameStringIfNoPageFragments() {
        String url = "http://www.google.com/about-us";
        assertThat(removePageFragments.apply(url)).isEqualTo(url);
    }

    @Test
    public void shouldRemovePageFragmentsIfPresent() {
        String url = "http://www.google.com/about-us#contact-us";
        assertThat(removePageFragments.apply(url)).isEqualTo("http://www.google.com/about-us");
    }
}