package com.sitemap.generator.crawler;

import com.sitemap.generator.util.LinkExtractorHelper;
import com.sitemap.generator.util.WebPageDownloadHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class WebCrawlerTest {

    @InjectMocks
    private WebCrawler webCrawler;

    @Mock
    private LinkExtractorHelper linkExtractorHelper;

    @Mock
    private WebPageDownloadHelper webPageDownloadHelper;

    @Test
    public void shouldReturnAllURLsCrawled() {

        String domain = "http://wiprodigital.com";
        String url1 = "http://wiprodigital.com/join-our-team/";
        String url2 = "http://wiprodigital.com/who-we-are/";
        String url3 = "http://wiprodigital.com/get-in-touch/";
        String externalURL = "https://twitter.com/wiprodigital";
        String relativeURL = "/products/machine-learning-platform";
        String mediaLink = "http://17776-presscdn-0-6.pagely.netdna-cdn.com/wp-content/uploads/2016/06/blueprint-e1464977489331.png";
        String pdfURL = "http://www.cis.upenn.edu/~lee/07cis505/Lec/lec-ch1-DistSys-v4.pdf";

        String rootHTMLString = "rootHTML.html";
        String joinOurTeamHTML ="joinOurTeamHTML.html";
        String whoWeAreHTML = url3 + " \n ";
        String machineLearningHTML = "machine_learning.html";

        when(webPageDownloadHelper.isSameDomain(url1,domain)).thenReturn(TRUE);
        when(webPageDownloadHelper.isSameDomain(url2,domain)).thenReturn(TRUE);
        when(webPageDownloadHelper.isSameDomain(url3,domain)).thenReturn(TRUE);
        when(webPageDownloadHelper.isSameDomain(relativeURL,domain)).thenReturn(TRUE);

        when(linkExtractorHelper.isHTTPLink(domain)).thenReturn(TRUE);
        when(linkExtractorHelper.isHTTPLink(url1)).thenReturn(TRUE);
        when(linkExtractorHelper.isHTTPLink(url2)).thenReturn(TRUE);
        when(linkExtractorHelper.isHTTPLink(url3)).thenReturn(TRUE);
        when(linkExtractorHelper.isHTTPLink(externalURL)).thenReturn(TRUE);
        when(linkExtractorHelper.isHTTPLink(mediaLink)).thenReturn(TRUE);
        when(linkExtractorHelper.isHTTPLink(pdfURL)).thenReturn(TRUE);
        when(linkExtractorHelper.isHTTPLink(domain+relativeURL)).thenReturn(TRUE);

        when(webPageDownloadHelper.isMediaLink(pdfURL)).thenReturn(TRUE);
        when(webPageDownloadHelper.isMediaLink(mediaLink)).thenReturn(TRUE);

        when(webPageDownloadHelper.isRelativeURL(relativeURL)).thenReturn(TRUE);

        when(webPageDownloadHelper.getWebPageAsHTMLString(domain)).thenReturn(Optional.of(rootHTMLString));
        when(linkExtractorHelper.extractHyperLinks(rootHTMLString)).thenReturn(new HashSet<>(asList(url1, mediaLink)));


        when(webPageDownloadHelper.getWebPageAsHTMLString(url1)).thenReturn(Optional.of(joinOurTeamHTML));
        when(linkExtractorHelper.extractHyperLinks(joinOurTeamHTML)).thenReturn(new HashSet<>(asList(url2, externalURL,relativeURL)));

        when(webPageDownloadHelper.getWebPageAsHTMLString(url2)).thenReturn(Optional.of(whoWeAreHTML));
        when(linkExtractorHelper.extractHyperLinks(whoWeAreHTML)).thenReturn(new HashSet<>(singletonList(url3)));

        when(webPageDownloadHelper.getWebPageAsHTMLString(url3)).thenReturn(Optional.empty());

        when(webPageDownloadHelper.getWebPageAsHTMLString("http://wiprodigital.com/products/machine-learning-platform")).thenReturn(Optional.of(machineLearningHTML));
        when(linkExtractorHelper.extractHyperLinks(machineLearningHTML)).thenReturn(new HashSet<>(singletonList(pdfURL)));

        Set<String> urlList = webCrawler.crawl(domain);

        assertThat(urlList).containsExactlyInAnyOrder(url1, url2, url3, externalURL,pdfURL, domain + relativeURL,mediaLink );
    }
}
