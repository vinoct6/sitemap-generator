# Sitemap Generator

Sitemap generator is an application to generate a sitemap.


### Requirements:

JDK 1.8, Maven 3.3+

### Tech

####This project primarily uses these projects and platforms.
-  Spring Boot 
-  JUnit and Mockito
-  AspectJ
-  Apache commons StringUtils and IOUtils
-  Pivotal Cloud Foundry

### Starting the project

I have pushed a chicken utility script to do various operations (only for bash / zsh shells)
This is just a convenience script for organizing the frequently used commands in one place.

- **./chicken backend_units**   -  Run the backend unit tests
- **./chicken recompile**       -  Recompiles the application
- **./chicken start_app**       - Starts the application in embedded tomcat server in port 8080
- **./chicken recompile_start** - Recompile and start the application

If the application running on Windows, you could just use plain maven commands

- **mvn clean package** - will run and package the application
- **mvn test**  - will run backend tests
- **java -jar target/sitemap-generator.jar** - will start the application in 8080 port

*You could start the application in another port by exporting the SERVER_PORT env property*.

## About the application:

Given the domain URL, this application visits all the http links in the same domain and keeps adding the visited links to a List.If the http url visited is a media file or an external domain, the application just adds it to the list, but doesn't go ahead and crawl it.

It is a simple, single threaded application. The application extracts the links form a web page and adds all the http links from the same domain to a queue and repeats the same process until the queue is empty.

It also exposes a REST API, http(s)://{domain}/crawl?url={url}, where url is the domain to crawl.
For example, to crawl wiprodigital.com, you could say

```
http://localhost:8080/crawl?url=http://wiprodigital.com/
```

Also, for demo purposes, I have deployed the application to Pivotal Cloud Foundry. 
This is the URL in which the application is deployed with a sample for crawling wiprodigital.com

```
https://sitemap-gen.cfapps.io/crawl?url=http://wiprodigital.com/
```

**Once this is invoked, check the server logs to see the progress.**

# Next steps :
#### There are number of improvements that can be made:
- The application is single threaded and therefore will take lot of time to crawl sites with deep links. To solve this problems, several websites have 
time limit on them, for example, they will crawl only for 30s and report whatever they found in that time. 
- We could introduce concurrency in our application. This is a variant of Producer consumer problem, so we could modify our architecture to accomodate our design.
-   Feedback to browser - once the URL is invoked in browser, there is no feedback, and the user is left wondering what is happening- we have to check the server logs for progress.
- Adds support for download the links in different formats ( xml, json etc )
- Adds support for compressing and downloading the file containing links